
public class MatrixCalculator {

	public static void main(String[] args) {
		int[][] matrix1 = {{6, 5, 3}, 
						{8, 1, 9}, 
						{7, 4, 0}} ;
		
		int[][] matrix2 = {{6, 5, 7}, 
				{8, 1, 9}, 
				{7, 4, 0}} ;
		
		int[][] sum = add(matrix1, matrix2);
		
		for (int i =0 ; i<sum.length; i++) {
			int[] element = sum[i];
			for (int j = 0; j < element.length; j++) {
				System.out.print(element[j]+" ");
			}
			System.out.println();
		}
	}

	
	public static int[][] add(int[][] matrix1, int[][] matrix2){
		int[][] result = new int[3][3];
		for (int i =0 ; i<3; i++) {
			
			for (int j = 0; j < 3; j++) {
				result[i][j]= matrix1[i][j] + matrix2[i][j];
			}
		}		
		return result;
	}
}
