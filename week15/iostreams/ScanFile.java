package iostreams;

import java.io.*;
import java.util.Scanner;

public class ScanFile {
	public static void main(String[] args) throws IOException {
		Scanner s = null;
		try {
			s = new Scanner(new BufferedReader(new FileReader("week15/Xanadu.txt")));
			s.useDelimiter("\\W+");
			while (s.hasNext()) {
				System.out.println(s.next());
			}
		} finally {
			if (s != null) {
				s.close();
			}
		}
	}
}
